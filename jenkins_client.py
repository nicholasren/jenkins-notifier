import jenkins
import ssl

def disable_ssl_verification():
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        pass
    else:
        ssl._create_default_https_context = _create_unverified_https_context

def jobs(view):
    return __server().get_jobs(view_name = view)

def is_failed(job):
  return job['color'] == 'red' or job['color'] == 'red_anime'

def build_succeed_for(view):
    disable_ssl_verification()
    return not any(map(is_failed, jobs(view)))

def queue_length():
    queue_info = __server().get_queue_info()
    return len(queue_info)

def __server():
    disable_ssl_verification()
    return jenkins.Jenkins('https://babylon-ci.engr.acx')
