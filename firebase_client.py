from firebase import firebase

FIREBASE_URL = "https://jenkins-notifier-47ef4.firebaseio.com/"
firebase = firebase.FirebaseApplication(FIREBASE_URL, None)

def saved_status_for(view):
  return firebase.get('/status/' + view , None)

def mark_as_failed(view):
  firebase.put('/status/', view, {'was_failed': True})

def mark_as_succeed(view):
  firebase.put('/status/', view, {'was_failed': False})
