import requests
import json

url = 'https://hooks.slack.com/services/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'

def payload_with(emoji, msg, link_names):
    payload = {
                'channel': '#build-notifier',
                'username': 'jenkin notifier',
                'icon_emoji': emoji,
                'text': "@here " + msg,
                'link_names': link_names
            }
    return json.dumps(payload)


def notify_success():
    requests.post(url, data = payload_with(':green_heart:', "BUILD PASSED! :green_heart:", 2))

def notify_failure():
    requests.post(url, data = payload_with(':broken_heart:', "BUILD_FAILED! :broken_heart:", 2))

def notify_large_queue(size):
    message = "There're still %s jobs in the queue, please hold your push" % size
    requests.post(url, data = payload_with(':yellow_heart:', message, 1))


