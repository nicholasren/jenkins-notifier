

#### Requirements:
- subscribe
  ```
  <subscriber>: /subscribe to <view>
  ```
- unsubscribe
  ```
  <subscriber>: /unsubscribe from <view>
  ```

#### Data store[dynamo]

Subscriptions:
```
{
  view_name: build_radiator,
  subscribers: ['xiaojunr', 'molivera']
}
```
